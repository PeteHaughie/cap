import ipcapture.*;
import camera3D.Camera3D;
import camera3D.generators.*;

IPCapture video0;
IPCapture video1;
Camera3D camera3D;

float rot = 75f;
float rotX = rot * 0.5f;
float rotY = rot * 0.1f;
float rotZ = rot * 0.3f;

void setup(){
  size(640, 240, P3D);
  camera3D = new Camera3D(this);
  BarrelDistortionGenerator generator = camera3D.renderBarrelDistortion().setDivergence(1);
  generator.setBarrelDistortionCoefficients(0.22, 0.24); // 0,0 is no distortion
  camera3D.enableSaveFrame('s', "debug");
  video0 = new IPCapture(this, "http://192.168.0.7:8081", "", "");
  video0.start();  
  video1 = new IPCapture(this, "http://192.168.0.7:8081", "", "");
  video1.start();  
}

void preDraw(){
  rot += 1;
  rotX = rot * 0.5f;
  rotY = rot * 0.1f;
  rotZ = rot * 0.3f;
}

void draw(){
  if (video0.isAvailable()) {
    video0.read();
    image(video0,0,0);
  }
  if (video1.isAvailable()) {
    video1.read();
    image(video1,320,0);
  }
}