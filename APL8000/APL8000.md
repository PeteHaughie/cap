# APL8000 Conceptualising Landscape

## Postcards from the Edgelands #1

Dr Thompson,
I was taken by your comments about the propaganda and the socialisation of the North of England by the Romantacist Picturesque movement.

It put me in mind of the folk horror movement and the haunted landscape, full of ghosts and goblins. I couldn't put let go of the idea that the North of England had been turned into a ghost train, a precursor to the gentrification to come. An inflated sense of danger in what was probably a perfectly lovely place to live for the indigenous Northerners.

If language changed the view of the landscape with its hyperbole I wondered if it were possible to extract what the poets were seeing by setting a wet flannel to the fire of their words.

Dr Thompson, I present 'Words Unworthy'

I walked alone,
across the ground,
when I saw,
some flowers.
Near some water.
It was a bit windy.

In a straight line,
right on the water edge,
There were a lot of them.
It was a bit windy.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #2

Dr Thompson,

I remember thumbing through Robert Hughes' Shock of the New back in the mid 90s and reading about Corbusier's Cité radieuse and Plan Voisin. His ideas for Paris were thrilling for a teenager obsessed with science fiction. The Unité d'Habitation that was eventually constructed in Marseille and almost abandoned shortly after its construction due to its inhumanity is now sought after by middle class professionals and is a listed UNESCO site. I can only presume that the new residents have never read Burrough's High Rise. But much like pastoral afterlife idolising the drudgery of farm labour maybe the only thing that turns hell into heaven is time.

I was going to compare Gotham to Mega City One but instead how about something from a simulation? Previously featured in Vice and Wired magazines but luckily for the 9.2 virtual residents Magnasanti is only a hypothetical city hell. It may well be only an imaginary landscape but for all of that I'm reminded strongly of the aerial shots of Shanghai. It's definitely worth searching YouTube for. I wonder if the Shanghai's planners are aware of Magnasanti and if he in turn is aware of Cité radieuse? Perhaps Corbusier's dream will come true after all.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #3

Dr Thompson,

In the absence of a lesson this week I thought I'd take the time to tell you about Silvertown in London.

Somewhere along the North bank of the Thames, in East London, up towards Greenwich is a dying town. Now brooded over by the hulking presences of the Tate and Lyle sugar refinery and Knight animal rendering facility it was actually named for the rubber factory that opened there in the middle of the nineteen hundreds by Samual Silver.

It was a prosperous area once, full of the hustle and bustle of the sort of industry you wouldn't want too close to the city. In its own way it thrived though. Until 1917 when it was the site of an enormous explosion at a munitions factory when 50 tonnes of TNT being stored for the war effort went up. The explosion killed 73 workers and injured 400 more. The explosion also devastated large swathes of the area, 70,000 properties were damaged, 900 utterly. The area has never been rebuilt.

The area's last pub, Cundy's Tavern, closed this year, a most British of tragedies.

It's a strange place. Less a destination and more a liminal space that exists at the periphery of perception. A mirage only seen from public transport. It won't stay this way of course, like every part of London it's being redeveloped and will be a sterile forest of giant glass trees in less than ten years.

Wish you were here!

Pete Haughie  

## Postcards from the Edgelands #4

Dr Thompson,

Back to the haunted landscape if we may for a moment. For Elgar to say, and I paraphrase, "If you hear the theme from my concerto in the hills when you're walking, don't worry. It's only me." Elgar has effectively forever haunted the landscape in the imagination of his friend. Forever. I can't decide if that's a comforting or incredibly distressing thing to have done. I appreciate it nonetheless.

I'm still taken with the mental image of Byron laying prone in that coffin boat in the Devil's Arse. There's something distinctly Stygian about it. All that was missing were the coins on his eyes for Charon.

Thank you for the introduction to Edwin Butler Baylis' work. Through it I was reminded of a young Swedish painter named Simon Stålenhag. He has this very strange approach to the Swedish landscape, taking the wilderness, farming, urban, industrial or what have you and reframing them in a context of science fiction, adding dramatic vignettes which results, I think, in a very exciting moment in representational painting.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #5

Dr Thompson,

From a collection of doors left over from a recent renovation, some fences from my mother's gardening efforts and some offcuts of carpet my father built me an honest to goodness shack of my own at the bottom of the garden. It nestled under a bank of firs that grew as a separator between us and our neighbours and was just about the right size to shelter me, our dog, and my comic collection.

Ever since you asked us, rhetorically, if photography could ever rise above the topographical I've been trying to find an artist that fitted the bill. Nothing immediately came to mind but as I was reading Christiane Paul's book Digital Art I think I might have found one. Dieter Huber is an Austrian artist who, among other things, combines his landscape photographs with digital techniques to create incredibly believable artificial vistas.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #6

Dr Thompson,

Your slides of the gardens of the Palace of Versailles put me strongly in mind of Goetian sigils and geomantic patterns. Yet no matter how much I wanted there to be a connection to that world I couldn't find one. Perhaps the connection is only there in an imagined Palace where strange magic roams the halls as it did in Prague castle when John Dee and Edward Kelley. I did however stumble upon the story of two English lecturers from Oxford who, in 1901, wrote an account of having stumbled back in time, watching normal 1780s life and behaviour, observing some then demolished buildings, and even meeting Marie Antoinette as a 19 year old at the 'Petit Trianon'. A strange landscape to inhabit not only the present but the past simultaneously. If of course it even happened…

You can read their account in the book 'An Adventure' by Anne Moberly and Eleanor Jourdain.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #7

Dr Thompson,

I write this to you from the confusing anachronism that is Venice. The city sits on 118 manmade islands constructed in about 150 AD from Slovenian alder and Istrian limestone by the refugees fleeing the sacking of Rome. For two thousand years of continuous habitation (at least!) the old girl's looking all right. I get the feeling that if you looked away for too long this place would silently slip beneath the waters and that it's only by the constant attention of her loving attendants that she remains propped up, slowly sagging beautifully outwards.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #7.5

Dr Thompson,

I finally managed to catch up with the viewing for Utopias. From our previous correspondence hopefully you'll know I'm at least partially familiar with some of the buildings mentioned in the film. Others were entirely new to me. I'll be honest I was blown away more than once. Some of those buildings and concepts were so high level. Corbusier looked like he was phoning it in by comparison.

I think there's a particular piece to camera near the end that will resonate with me for a good long while, although I paraphrase: "There's no such thing as a utopia. Anything we build will be a straitjacket for our children".

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #8

Dr Thompson,

Scotland. I flew out of Edinburgh last week. Before boarding my flight I decided I'd get a bite to eat and look out over the land. The place was called The Gathering. I think you can imagine how hard I rolled my eyes. Despite that I managed to get a good half hour or so watching the planes land, the strange little vehicles doing their busywork and the trains threading through the hills in the distance.

I have so many stories or Scotland and 'Scottishness' that it's hard to choose one of my own so I'm going recount one secondhand from a good friend of mine, a Canadian called Naomi. She grew up in Vancouver, which by all accounts is quite quiet, especially for teens and young adults. One of the few social activities that was available that everybody seemed to be keen to do was cèilidh. Specifically the social dancing. No, I don't get the appeal either.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #9

Dr Thompson,

Wilderness and rewilding… Such a difficult subject to weigh in on. Of course I would love to see a return to a greener, more wooded Britain, replete with appropriate wildlife, boars, lynx, wolves, bears even but I'm also aware that such a romantic thought comes only with privilege. The last lynx was killed 1600 years ago, last bear in the UK was killed over 1000, the last wolf 400. Boars have been successfully reintroduced after their previous extinction in 1400 although only in some areas.

If we do decide to 'rewild' the British Isles then the species will be foreign to today's Britain. I overheard a couple of academics on the Metro, only last week in fact, who were discussing the subject of the reintroduction of beaver into the Scottish Highlands. Apparently on the day of release they set about doing what beavers do best, making dams. Within a week waterways had been diverted and the entire eocsystem was being altered which had farmers up in arms. All for the sake of a few breeding pairs of beaver. Similarly there's a famous TED talk by George Monbiot about how wolves can alter the course of rivers in what, I have since come to learn, is known as a 'trophic cascade'.

I wonder how the British Isles would change with the reintroduction of a whole host of European apex predators? Perhaps it would alter us as much as it would the landscape and it's wildlife.

Wish you were here!

Pete Haughie

## Postcards from the Edgelands #10

Dr Thompson,

It was with great difficulty that I lifted my pen this week to take on the subject of global warming. I try not to think about it too much as it only makes me incredibly depressed when I do. The havoc we have and are wreaking on the global ecosystem seems innevtiable if you look back at our industrial heritage, population explosion, and rampant late-stage capitalism. I realise that this is the fallacy of hindsight, the ability to tie a connecting thread to previous events and mark them out as significant and obvious. Perhaps in the future the computer revolution of the 1980s and the subsequent rapid release cycles of modern machines such as tabletrs and phones with their planned obsolescence of today will be a point as obvious as the invention as the Model T Ford or the introduction of the coal power stations are to us now.

As for reversing or stalling global warming? I fear it's a case of too far in blood soaked ore tread we and that we're simply going to have to weather the worst of it whilst eco-tech matures and hopefully gains major adoption thereby allowing the planetary climate to regain some sort of balance. No pun intended.

I agree with your sentiment that the more sci-fi a concept appears the less hope I hold in its success and fear in its application. A reduction in consumption and waste is a given but I think it's going to have to be a whole host of smaller more manageable and thereby easier to correct, finesse, or reverse approaches that will sway the struggle in our favour. Of course this is nothing to say of the other societal and political changes that have to be adopted alongside this new green future.

Wish you were here!

Pete Haughie
