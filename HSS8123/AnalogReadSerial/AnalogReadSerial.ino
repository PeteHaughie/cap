/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogReadSerial
*/

// globals
int LDRValue  = analogRead(A0);
int pot1Value = analogRead(A1);
int pot2Value = analogRead(A2);
int pot3Value = analogRead(A3);
int pot4Value = analogRead(A4);

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  LDRValue = analogRead(A0);
  int mapLDRValue = map(LDRValue, 200, 900, 50, 1000); // map(source, fromLow, FromHigh, toLow, toHigh);
  if (mapLDRValue < 50){
    mapLDRValue = 50;
  }
  if (mapLDRValue > 1000) {
    mapLDRValue = 1000;
  }
  pot1Value = analogRead(A1);
  int mapPot1Value = map(pot1Value, 0, 1023, 0, 100);
  pot2Value = analogRead(A2);
  int mapPot2Value = map(pot2Value, 0, 1023, 0, 50);
  pot3Value = analogRead(A3);
  int mapPot3Value = map(pot3Value, 0, 1023, 0, 3000);
  pot4Value = analogRead(A4);
  int mapPot4Value = map(pot4Value, 0, 1023, 0, 1000);
  Serial.println(mapLDRValue + String(" ") + mapPot1Value + String(" ") + mapPot2Value + String(" ") + mapPot3Value + String(" ") + mapPot4Value);
  delay(1); // delay in between reads for stability
}
