import processing.serial.*;
import processing.video.*;

Serial myPort;        // The serial port

Capture cam;

int imgHeight = 720;
int imgWidth = 1280;
int offsetHeight = imgHeight / 2;
int offsetWidth = imgWidth / 2;
int LDR = 0;
int LDRprev;
int pot1 = 0;
int pot1prev;
int pot2 = 0;
int pot2prev;
int pot3 = 0;
int pot3prev;
int pot4 = 0;
int pot4prev;

void setup () {
  // set the window size:
  fullScreen(P3D);
  //size(640, 360, P3D);
  colorMode(RGB, 255);
  //noCursor();

  // List the available cameras
  String[] cameras = Capture.list();

  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    for (int i = 0; i < cameras.length; i++) {
      println(str(i) + " " + cameras[i]); // <- use this to debug your cameras
    }

    //The camera can be initialized directly using an 
    //element from the array returned by list():
    cam = new Capture(this, cameras[15]);
    cam.start();
  }

  // List all the available serial ports
  // I know that the first port in the serial list on my mac
  // is always my  Arduino, so I open Serial.list()[0].
  // Open whatever port is the one you're using.
  myPort = new Serial(this, Serial.list()[2], 9600);
  // don't generate a serialEvent() unless you get a newline character:
  myPort.bufferUntil('\n');
  frameRate(30); // do this at the end or the P3D jogl throws a hissy fit
}

void draw () {
  imageMode(CENTER);
  background(0);
  if (cam.available() == true) {
    cam.read();
    translate(width/2, height/2);
    rotateZ(radians(pot1));
    // rotateZ(radians(LDR));
    tint(pot2, pot3, pot4);
    image(cam, 0, 0, width, height);
   // loadPixels();
   // for (int x = 0; x<width; x++) {
   //   for (int y = 0; y<height; y++) {
    //    int loc = x+y*width;
    //    float r = red(cam.pixels[loc]);
    //    float g = green(cam.pixels[loc]);
     //   float b = blue(cam.pixels[loc]);
       // pixels[loc]= color(r, b, g);
     // }
    //}
    updatePixels();
    if (LDR<100) {
      filter(POSTERIZE, 4);
    }
    //loadPixels()
    //PImage camR = cam.get(0, 0, width, height); 
    //PImage camG = cam.get(0, 0, width, height); 
    //PImage camB = cam.get(0, 0, width, height); 
    //set(0, 0, cam); // <- faster but no transformations
  }
}

void serialEvent (Serial myPort) {
  // get the ASCII string:
  String inString = myPort.readStringUntil('\n');

  if (inString != null) {

    String[] list = split(inString, ' ');

    if (list[0] != null) {
      LDRprev = LDR;
      LDR = int(list[0]);
    }
    if (list[1] != null) {
      pot1prev = pot1;
      pot1 = int(list[1]);
    }
    if (list[2] != null) {
      pot2prev = pot2;
      pot2 = int(list[2]);
    }
    if (list[3] != null) {
      pot3prev = pot3;
      pot3 = int(list[3]);
    }
    if (list[4] != null) {
      pot4prev = pot4;
      pot4 = int(list[4]);
    }
  }
}