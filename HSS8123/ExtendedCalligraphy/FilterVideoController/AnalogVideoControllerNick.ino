/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogReadSerial
*/

// globals
int LDRValue  = analogRead(A0);
int pot1Value = analogRead(A1);
int pot2Value = analogRead(A2);
int pot3Value = analogRead(A3);
int pot4Value = analogRead(A4);

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  LDRValue = analogRead(A0);
  if (LDRValue < 460) {
    LDRValue = 460;
  }
  if (LDRValue > 900) {
    LDRValue = 900;
  }
  int mapLDRValue = map(LDRValue, 460, 900, 0, 255);
  pot1Value = analogRead(A1);
  if (pot1Value < 0) {
    pot1Value = 0;
  }
  if (pot1Value > 1000) {
    pot1Value = 1000;
  }
  int mapPot1Value = map(pot1Value, 0, 1000, -360, 360);
  
  pot2Value = analogRead(A2);
  if (pot2Value < 0) {
    pot2Value = 0;
  }
  if (pot2Value > 1000) {
    pot2Value = 1000;
  }
  int mapPot2Value = map(pot2Value, 0, 1000, 0, 255);

  pot3Value = analogRead(A3);
  if (pot3Value < 0) {
    pot3Value = 0;
  }
  if (pot3Value > 1000) {
    pot3Value = 1000;
  }
  int mapPot3Value = map(pot3Value, 0, 1000, 0, 255);

  pot4Value = analogRead(A4);
  if (pot4Value < 0) {
    pot4Value = 0;
  }
  if (pot4Value > 1000) {
    pot4Value = 1000;
  }
  int mapPot4Value = map(pot4Value, 0, 1000, 0, 255);

  Serial.println(mapLDRValue + String(" ") + mapPot1Value + String(" ") + mapPot2Value + String(" ") + mapPot3Value + String(" ") + mapPot4Value + String(" "));
  delay(25); // delay in between reads for stability
}
