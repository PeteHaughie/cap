/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogReadSerial
*/

// globals
int LDRValue  = analogRead(A0);
int pot1Value = analogRead(A1);
int pot2Value = analogRead(A2);
int pot3Value = analogRead(A3);
int pot4Value = analogRead(A4);

// set up the buttons
int digi1Pin = 7;
int button1State = 0;
int active1State = 0;
int digi2Pin = 8;
int button2State = 0;
int active2State = 0;

void setup() {
  // pinMode
  pinMode(digi1Pin, INPUT_PULLUP);  
  pinMode(digi2Pin, INPUT_PULLUP);  
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
  // digital stuff
  button1State = digitalRead(digi1Pin);
  if (button1State == LOW) {
    active1State = 1;
  }

  if (button1State == HIGH) {
    active1State = 0;
  }

  button2State = digitalRead(digi2Pin);
  if (button2State == LOW) {
    active2State = 1;
  }

  if (button2State == HIGH) {
    active2State = 0;
  }

  // analogue stuff
  LDRValue = analogRead(A0);
  if (LDRValue < 700) {
    LDRValue = 700;
  }
  if (LDRValue > 900) {
    LDRValue = 900;
  }
  int mapLDRValue = map(LDRValue, 700, 900, 0, 255);

  pot1Value = analogRead(A1); // main volume
  if (pot1Value < 0) {
    pot1Value = 0;
  }
  if (pot1Value > 1000) {
    pot1Value = 1000;
  }
  int mapPot1Value = map(pot1Value, 0, 1000, 0, 100);
  
  pot2Value = analogRead(A2); // effect volume
  if (pot2Value < 0) {
    pot2Value = 0;
  }
  if (pot2Value > 1000) {
    pot2Value = 1000;
  }
  int mapPot2Value = map(pot2Value, 0, 1000, 0, 100);

  pot3Value = analogRead(A3);
  if (pot3Value < 0) {
    pot3Value = 0;
  }
  if (pot3Value > 1000) {
    pot3Value = 1000;
  }
  int mapPot3Value = map(pot3Value, 0, 1000, 0, 100);

  pot4Value = analogRead(A4);
  if (pot4Value < 0) {
    pot4Value = 0;
  }
  if (pot4Value > 1000) {
    pot4Value = 1000;
  }
  int mapPot4Value = map(pot4Value, 0, 1000, 0, 100);

  Serial.println(mapLDRValue + String(" ") + mapPot1Value + String(" ") + mapPot2Value + String(" ") + mapPot3Value + String(" ") + mapPot4Value + String(" ") + active1State + String(" ") + active2State);
  delay(25); // delay in between reads for stability
}
