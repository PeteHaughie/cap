// globals
int ledPin = 13;
int minBlink = 100;
int maxBlink = 500;
int blinkDuration = 500;

void RNG(){
  blinkDuration = random(minBlink, maxBlink);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, HIGH);
  RNG();
  delay(blinkDuration);
  digitalWrite(ledPin, LOW);
  RNG();
  delay(blinkDuration);
}
