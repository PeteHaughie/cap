# MUS8160 Proposal

## Hum

I propose performing several documented unplanned sound walks throughout Newcastle and surrounding areas to uncover sounds through field recording, contact microphones, or other appropriate method to uncover the subject. All sounds will be treated as representational images or snapshots and not recorded performances or actions. The journey to and from each point where the sounds were encountered will be documented as well as the sound and its position, both in real world terms such as timestamps and geolocational data but also a psychogeographical descriptive account of the properties of the sound and space. All spaces will be public as this project is not interventional.

Initially these uncovered sounds will form the core of a library of resources for performance or durational works.

### Expedition 0

This expedition is not part of the final piece, more an exploration of a practice yet to be formed. There were two recordings made in public. Both recordings were ten minutes each as that's the shortest long period of time I could think of. It is somewhat arbitrary but for now it works conceptually for me as siz of them return something album-like in length which doesn't feel like an unpleasant amount of time to be listening to field recordings. The walk is not recorded as I think the act of walking and discovering places is more interesting than the journey itself.

Initially I had thought of starting at the Central Newcastle Station as it seemed a logical starting point, from the center moving outwards but on reflection I feel, much like a needle returns to the centre of a record, that the centre is where the expeditions should end. I will be choosing distant places from that central nexus point (reference intended) and navigating back on foot to discover what I can in between. I haven't decided if the starting points should be inside a specific zone or not yet.

For now I have chosen to gather my geolocational position via what3words.com as it's easier to store in the interim but will also use the returned mnemonic phrase as a series of longitudinal and latitudinal values and title.

#### Track 1
donor.little.weeks

54.976530, -1.614859

#### Track 2
assets.dairy.stole

54.976539, -1.614877

### Expedition 1
They say that every journey starts with a first step. In this case that first step is from my front door.
