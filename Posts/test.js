document.addEventListener("DOMContentLoaded", function() {
  var arrP = $("#no_content p");
  var arrPLen = arrP.length;
  var arrPCur = 0;
  var arrT = $(arrP[arrPCur]).text();
  var arrTLen = arrT.length;
  var arrTCur = 0;
  var cookie = document.cookie;
  var arrCookie = cookie.split(" ");
  var matchCookie = false;

  $("#no_content").attr("style", "text-align: center; font-family: monospace;");

  for (var i = 0; i < arrCookie.length; i++) {
    if (arrCookie[i] == "no_content" || arrCookie[i] == "no_content;") {
      matchCookie = true;
    }
  }

  if (matchCookie === true) {
    replaceAllNow();
  } else {
    replaceAllSlow();
  }

  function replaceAllNow() {
    for (var j = 0; j < arrPLen - 1; j++) {
      arrT = $(arrP[j]).text();
      for (var n = 0; n < arrT.length; n++) {
        $(arrP[j]).text($(arrP[j]).text().replace(arrT[n],"."));
      }
    }
  }

  function replaceAllSlow() {
    var interval = setInterval(function(){
      if ((arrPCur < arrPLen - 1) && (arrTCur < arrTLen)) {
        $(arrP[arrPCur]).text($(arrP[arrPCur]).text().replace(arrT[arrTCur], "."));
        arrTCur++;
      } else if ((arrPCur < arrPLen - 1) && (arrTCur == arrTLen)) {
        arrPCur++;
        arrT = $(arrP[arrPCur]).text();
        arrTLen = arrT.length;
        arrTCur = 0;
      } else {
        clearInterval(interval);
        document.cookie = "no_content";
      }
    }, 1000);
  }

});
