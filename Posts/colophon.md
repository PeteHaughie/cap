# Colophon

> Link Rot: The process by which hyperlinks on individual websites or the Internet in general point to web pages, servers or other resources that have become permanently unavailable.

> Software Rot: Term used to describe the tendency of software that has not been used in a while to lose; such failure may be semi-humorously ascribed to bit rot. More commonly, software rot strikes when a program's assumptions become out of date.

> Binaries: [O]f, relating to, or written in binary code; programmed or encoded using only the digits 0 and 1: All executable programs on the computer are stored in binary files.

## The Problem

When Tim Berners Lee first built a system for connecting electronic documents which existed in different geographical locations together via hyperlinks he accidentally also introduced the possibility for the breaking of that link. Paul Virilio sums this up well:

> When you invent the ship, you also invent the shipwreck; when you invent the plane you also invent the plane crash; and when you invent electricity, you invent electrocution… Every technology carries its own negativity, which is invented at the same time as technical progress.

Recently a friend of mine sent me a link via a Facebook memory from seven years ago that I had previously sent them. I was excited to relive that memory but when I clicked that link, although the article was still present, unfortunately the video was now unplayable. It was only available as a Macromedia Flash media file with no other option of formats. Flash is no longer a trusted format to use online as it has security flaws and therefore is not installed by default. This is true currently of Chrome `Version 61.0.3163.100 (Official Build) (64-bit)` for Mac OS `Version 10.13 (17A405)` and is even unavailable for download and install if I were willing to take that risk. The file is present but the contents are out of reach.

Another friend of mine is a musician now living in Berlin who moved all of his studio equipment from New York. He found a large box of Digital Audio Tapes `DAT` from the 90s and early 00s in amongst his things. He was disappointed to find that no matter what he tried that he could not load a single one of them. The media and files are present but the environment for retrieval has been lost.

To compound his problem unless he gets that data from those tapes in one way or another then he'll have the added risk of losing the opportunity to save the data these master tracks contain due to tape degradation. Tape degradation is the name of the process in which the magnetic substrate lifts from the plastic strip. Tape degradation is already a huge problem and likely to become more so over the coming years, not just for magnetic tapes but also computer diskettes and likely if not definitely all other magnetic media. The media is at risk of permanent loss.

I kept a copy of Apple's software DVD Studio Pro as a disk image so I could install it if I ever needed to open DVD scripting projects I made in 2010. I have a copy of the installation CD but CD drives on computers are now anachronistic. This software was discontinued in 2009. During the writing of this article I have realised that the image is in a `toast` format and might well also now be corrupted which denies me access to the contents of my DVD project files. Of course this is dependent on the installer still being compatible with the latest OS version. Much like the shift from PowerPC binaries, to Universal Cocoa 32 Bit binaries, and most recently to purely 64 Bit binaries even the tools of media creation become obsolete and lost through constant iterative forced upgrade paths. The file is present but the software is lost and environment has changed.

I am forced into performing forensic Media Archaeology of data of my own creation laid down less than a decade ago.

## A Discussion Break

The issue of digital archival is of ongoing concern to a few groups. These groups are trying extremely hard to ensure access to the data amassing in incredible quantities for the future. A lot of talk is made of the amount of data being created on a daily basis but I don't hear much about how much is lost in that same timeframe. Who will be the instigators, curators, custodians, and librarians of this incomprehensible digital library?

### Rhizome

Rhizome is a well respected and very visible archival project which is attempting to specifically save Internet Art or `net art`. They've gone so far as to create a quite of tools for that purpose alone and have released them for the public to use as well. Subjectively the most notable of these from the perspective of online digital art is [Web Recorder](!https://webrecorder.io/).
It does address a lot of the issues of link rot but not those of bit rot. Obviously it also does not preserve the server components and configuration as that is hidden from the browser.

### The Internet Archive and the Wayback Machine

The Archive Organisation helmed by Brewster Kahle has made great headway in archiving video and audio media as well as websites for posterity. Which, although not halting, has at least begun the task of offsetting the problem of link rot by storing the information… somewhere. For recall it uses the same link format and naming convention as the original so that one can simply search for a page address in the hope that it has been stored. If a page has not been archived yet and it's at risk of disappearing one is able to request a backup directly from the `no returned results` page. There are limitations such as if there are `nofollow` properties in the site's `robots.txt` it blocks attempts to `spider` the directory structure. Also javascript handling of links rather than the browser default functionality, based on the hyperlink, similarly ruin any attempt at retrieval so is by no means a perfect solution. Quite often javascript, stylesheets, images and other files such as music or video don't get archived dependent on the server configuration.

Perhaps if internet service providers and browsers began setting their 404 pages to point to the known search parameters with a query string constructed of permanently moved or deleted domains and targets to trawl the Internet Archive directly then overall link rot might be reduced.

### Hobbyists, Obsessive Collectors and Pirates (Oh My)

This really only applies to publicly available or commercially released software but deserves a minor mention. One way of finding older games, programs, or operating systems is via underground piracy scenes. The people who are fans of a particular computer systems are often completionists so finding a particular version of Amiga Workbench, which comes in several flavours for various sets of hardware profiles for example, is easier than attempting to find original copies of the disks or even backing up one's own. The distributed nature of these groups and the method of their peer to peer sharing is much like having a self-propagating off-site backup. One that may involve asking very politely of individuals who have copies of very rare things or being an active part of the community.

Not all groups of this sort are pirates of course, there are equally exciting retrieval endeavours in all sorts of interesting places. One such effort is one where arcade enthusiasts are attempting to restore the `bin` files of obsolete original arcade machines by milling the top from the `ROM` chips on the circuit boards and literally transcribing the `bits` to recreate the underlying program which can then be read, stored, examined and so on with less risk of it being lost forever.

You can read more about this really interesting project at [CAPS0ff](!https://caps0ff.blogspot.it/).

More examples of places to obtain legally available original software are [World of Spectrum](!http://www.worldofspectrum.org/), [DOSGamesarchive](!http://www.dosgamesarchive.com/), [Good Old Games](!https://www.gog.com/) and [Amiga Forever](!http://www.amigaforever.com/).

### Nostaligists and Niche Interest Groups

One of my favourite emergency archival projects was initiated when it was suddenly announced that the online community Geocities was being taken down. The GEOCITIES MIDI SWIPE swung into action to save a very specific part of Internet culture. Here is the contents of the README file that accompanies the resultant torrent.

```
[--__-_] A R C H I V E   T E A M [_-__--]

 The GEOCITIES MIDI SWIPE Version 1.0
       FLAT AND UNIQUE Version
    Compiled on November 10, 2009

On October 27, 2009, Yahoo! Inc. shut down the Geocities service, a collection of
free webpages with entries and hosted sites dating back to 1995. Along with this
shutdown was the destruction of millions of files and history, lost to the dustbin
of time.

...well, except what dozens of people from around the world were desperately
downloading as fast as they could. This band of archivists, including members of
Archive Team, Reocities, Geocities.ws, and many others, did their best to ensure
at least a portion of this collection of files would live on.

This is a quick and dirty collection of MIDI files taken from the Neighborhoods of
Geocities, given arbitrary unique names and sorted by first letter/character. MIDI
files could be summoned by browsers when visiting a website and could provide a
music soundtrack while looking at that page. The MIDI files themselves came from
a wide variety of sources, professional and amateur, and represent, in most cases,
commercial songs re-written for the MIDI format. Some have credits baked into the
files; others do not. Some are complicated orchestrations using the best of what
the MIDI format has to offer, while others are simplistic and repetitive.

Please note that many of these files have been renamed arbitrarily to allow all the
filenames to fit into flat directories, and should not be considered the actual
names used by the website authors when putting together their sites. A simple dupe
checking has occurred so the exact same file doesn't appear in multiple places,
although any changes to the file in any way over time would make it appear to be
different even though the music would not. Also note that copyright and creator's
rights still exist for both the music and the files themselves, and no use of
these files is implied or permitted by Archive Team's compiling of them.

In total, over 51,000 songs are located in this collection, spanning the full
1995-2009 lifetime of Geocities.
```

"The dustbin of time". That's quite the phrase to conjure with. I keep a copy of this archive on a USB thumbdrive around my neck. I envisage a day when we're all scrabbling to download our snaps from Facebook or Instagram after the servers of these giants of our age are at risk of going dark for good when the tide of fortune inevitably changes.

Emergency archival projects are related to other acts, quite often less benevolent or legal in nature such as the Panama and Paradise Papers, Wikileaks, and The Snowden Files. No less important but definitely only archival in terms of the need to stop powerful figures of our time from hiding illegal acts from scrutiny by propagating the contents of that data as far and wide as possible.

### A Quick Thought on Archival

Recently I was informed about the Adobe Museum of Digital Media. A year long project the result of which was a hyperreal online space with virtual curators and assistants which attempted to store and display early and new net art for viewing in a gallery space. The attempt to experiment in curation in the virtual realm is admirable but sadly it is now unavailable to view. The [last working snapshot of the museum](!https://web.archive.org/web/20121221051231/http://www.adobemuseum.com/) is from the 21st of December 2012 but unfortunately it is not accessible due to the requirement of the Flash browser plugin. The irony is tangible.

You can see a little of this project in what remains of other artists' and collaborators' documentation such as this Vimeo video from [Unit9](!https://www.unit9.com/project/adobe-museum-of-digital-media/).

## Solution?

There isn't one as yet. Not a complete one at least. There is some potential hope in a holistic approach though. Let me break it down.

The issue is non-trivial but perhaps the wider arts community could learn some best practice from the software engineering and book publishing industries to mitigate the worst excesses of it.

### Colophon

Prior to the 16th Century bookmakers employed a practice called colophony. This colophon encapsulated detailed information about the printing of a particular book.

> Colophon: [A]n inscription at the end of a book or manuscript, used especially in the 15th and 16th centuries, giving the title or subject of the work, its author, the name of the printer or publisher, and the date and place of publication.

Wikipedia has the following paragraph and example in the entry for colophons:

> With the development of the private press movement from around 1890, colophons became conventional in private press books, and often included a good deal of additional information on the book, including statements of limitation, data on paper, ink, type and binding, and other technical details. Some such books include a separate "Note about the type", which will identify the names of the primary typefaces used, provide a brief description of the type's history and a brief statement about its most identifiable physical characteristics.

You can see an example of a modern book colophon here at [www.wolframscience.com](!https://www.wolframscience.com/nks/colophon/).

Some websites use colophons to describe their coding standards and compliances. This is also achieved with site metadata and `doctype` declarations.

### Define a Schema

What do I mean by schema?

> Schema: An underlying organizational [sic] pattern or structure; conceptual framework.

In this instance I suppose what I am saying is that there might be an argument for the creation of a common vocabulary of terms for the encapsulation in time of digital works. It might not be applicable to your method of working or it might be that the practice is temporary and therefore not a good fit for archival and retrieval.

A good example of a digital work that should have been forgotten but has actually become quite widely reproduced and shared is William Gibson's poem Agrippa. It was initially shared on a digital disk in a couple of different editions but they shared the same overall feature: They were supposed to self-delete once they had been read. Once they had been opened a program would slowly overwrite all the data on the disk with zeros, effectively returning the disk to a blank state. Sadly there were a few of problems which were not foreseen. Firstly a few MIT students sneaked a VHS camera into the single private reading of the poem and then released it to their friends as part of a video tape trading ring. Secondly some people who had bought or otherwise obtained versions of the disk made duplicates using techniques such as blind disk copying, effectively making a very slow one for one copy of the contents of the disk on other media. This then allowed software crackers access to the contents which they then set about decompiling and removing the file encryption and destructive program which would allow for copies of the disk to be read freely by anyone. One of the main reasons for the artwork, for it to be a single shot application for limited audiences was ruined. For Gibson fans it was great but from an artistic point of view it was partly a failure. Full disclosure: If it hadn't failed I would not have been able to read it many years after its release.

In my case most works would probably take on an aspect similar to a crash report or system profile output where the OS build and hardware specifications are listed followed by the relevant application versions. For example a toy for the web might be something like:

```
Development:
  Hardware:
    Model Name:	MacBook Air
    Model Identifier:	MacBookAir6,2
    Processor Name:	Intel Core i5
    Processor Speed:	1.4 GHz
    Number of Processors:	1
    Total Number of Cores:	2
    Memory:	4 GB
    System Version:	macOS 10.13.1 (17B48)
  Software:
    Photoshop CS6 Version 13.0 x64
    Adobe Illustrator CS6 Version 16.0.0
    Github Atom 1.22.0

Platform:
  Target: Google Chrome Version 61.0.3163.100 (Official Build) (64-bit)
```

For hardware and software projects there might be diagrams of circuits or plans of housings as well but as I'm only trying to begin a dialogue that's better left as an exercise for the reader.

### Development Environment Stasis

Another interesting paradigm from the software world that might be of interest to the artist is that of creating a static development environment. Developers who use Ruby or Node realised that they sometimes ran into issues when they updated their system-wide binaries for their programming environments. These were often caused by the dependencies which were either not updated at the same speed as the core tool or not at all which would cause their project to no longer work. In this case dependencies refers to the plugin like structure of node, `npm`, and ruby apps, `gems`. To combat this tools were developed to invoke specific versions instead of the default core tool instead of the system default. With node it was the Node Version Manager (`NVM`) and with Ruby it was the Ruby Version Manager (`RVM`). Using these tools means that the chain of trust with the dependencies is maintained and the project can be run without issue.

All of these tools use something a called `lock file` which set the current state of the project dependencies at the version that exists at the moment that it is created. This is of use when installing the project from nothing or pushing to a server for staging, testing, or deployment.

They often take a JavaScript Object Notation `JSON` or YAML Ain't Markup Language `YAML` format.

``` JSON
{
  "name": "MyProject",
  "author": "Pete Haughie",
  "dependencies": {
    "dependencyName01": "~1.0.0",
    "dependencyName02": "^2.0.0"
  }
}
```

``` YAML
  name: MyProject
  author: Pete Haughie
  dependencies:
    dependencyName01: "~1.0.0"
    dependencyName02: "^2.0.0"
```

The same issues also exists in a slightly different form in Python. There are two versions of the Python programming language available for modern systems so the core system binary isn't the issue. Instead the problem lies in the dependencies themselves in the way that sometimes there are clashes in versions or the way that they access system resources. Virtual Environment (Virtualenv or Venv) is a virtualised environment local to the project where Python can safely store and instantiate the project dependencies.

Alongside venv Python also has a method of snapshotting an environment in the form of `pip freeze`. This helpfully compiles all of the necessary dependencies into a single executable binary blob. Everything up to and including the kitchen sink get pulled in unless you specifically request it *not* to do so.

### Virtualisation and Provisioning

> Virtualisation: [T]o create a virtual version of (a computer, operating system, data storage device, etc.), which is not itself an independent device but both works and appears to the user as a single, physical entity.

> Provisioning: Provisioning provides equipment, software or services to customers, users, employees or IT personnel and has contexts in computing, computer networking and telecommunications.

Another approach is to not just store the project dependencies but also to take a snapshot of the system itself. This is named virtualisation after the act of creating an entire virtual computer called a `client` that lives on a `host` system. If there are specific requirements for the machine, say a particular sound or video device, they can be provided in much the same way as you would a dependency via either a command line invocation flag or configuration file. This approach can only provide modern systems. An explanation of why that is the case follows.

## Emulation

> Emulation: [R]efers to the ability of a computer program in an electronic device to emulate (or imitate) another program or device.

> Computer Architecture: In computer engineering, computer architecture is a set of rules and methods that describe the functionality, organization, and implementation of computer systems. Some definitions of architecture define it as describing the capabilities and programming model of a computer but not a particular implementation. In other definitions computer architecture involves instruction set architecture design, microarchitecture design, logic design, and implementation.

How does Emulation differ from virtualisation? The distinction is subtle but significant. With a virtual system the computers don't have to have the same operating system but usually it's more efficient to have the same or similar `architecture` to allow native OS API calls directly to the Central Processing Unit `CPU`. This is not always possible. Before Intel IBM CPUs became the de facto standard for computer hardware there were several contenders for the crown. A notable example of this is the Motorola 68000 or 68K chipset family which were installed in everything from the last generations of Commodore computers such as the Amiga 500, 600, 2000, and 4000 to the PowerPC and Quadra Macintosh computers. These computers are now obsolete but rich in entertainment and art and design media due to their popularity with creative artists and media agencies. This media is unreachable with no access to a physical computer without emulation. Emulation is much more taxing on the host system than virtualisation due to having to convert the OS API calls to the chip via a model of the chip that exists purely in memory before attempting to execute instructions on it. Emulation is objectively not as true to the source as virtualisation due to the fact that the instructions aren't just being routed to a physical chip via an API and as such may be prone to incomplete or incorrectly implemented chipset features.

The list of emulated hardware is extremely extensive and I couldn't possibly go into too much detail here.

### A Quick Thought on Emulation

I think it's interesting that the backwards compatibility functionality of the Xbox One which provides access to Xbox 360 and first generation Xbox games is actually provided by encapsulated machine images. This image comes prepackaged with the game installed and is provided via emulation not virtualisation due to the major differences in the hardware across the generations. The raw horsepower of the host chipset is used to invisibly deliver an authentic experience from the past system on the more modern one through their shared interface, the controller.

Previous attempts at backwards compatibility, by Sony on their Playstation hardware platform for example, involved having a pared down duplicate chipset of the original system piggy backing on the system board. This was not true for all models of the PS2 and indeed due to their change to their hardware that they decided to pull the functionality in further hardware releases in the form of the PS3 and PS4.

## More War Stories

Unfortunately this idea relies on being able to find the exact version of the binaries you are dependent on. Again all the proof of a problem I can offer is anecdotal which implies that the problem is me. I hope I can assure you that in this case it is not.

Whilst attempting to update some system dependencies for a large website's test suite I was suddenly encountering failed feature tests. I would delete the dependencies, follow the install procedure and the tests that had been working the previous day were suddenly failing. It made no sense. So I went through each install step, one by one, download, delete, and repeat for each. Until I discovered that the headless browser at the remote end just wasn't there anymore. Unbeknownst to me there had been a major security issue discovered a few days previously and the vendor had completely removed the offending file from their servers. Luckily I was able to update to a more recent version and the tests still passed but this binary file redaction also happens by accident as a side effect of the link rot mentioned previously.

At the same company a good friend of mine who I have worked with previously and know to be an extremely talented developer was given the task of installing an older codebase on her new work machine. It was a Ruby based project and extremely complex and particular. She followed the instructions to the letter but try as she might it refused to install fully and run. The documentation was out-of-date. She had to completely rewrite and rediscover the correct method for installation for others to follow in the future. There had been a large turnover in the workforce in the months preceding her employment and the information for the task had been siloed and subsequently lost, never having made it as far as the README file.

A colophon.txt will only be as useful as the information it contains as it pertains to a working environment. Consider it more closely related to a preflight check where all of the assets are gathered before sending a project on to a printers. A crystalline snapshot.

## Caveats

I most definitely am not suggesting using this as a method of organising or trying to compartmentalise the art itself. Rather I'm trying to open discussion on a way of giving the artist or archivist a fighting chance of being able to preserve the working folder and file structures and software context for revisiting pieces further down the road. The longer one works in digital media the harder it is to retain all of the information one creates and accrues. With a colophon.txt or colophon.md with your files perhaps you'll have an understanding of the technological overview of the environment you or the artist once created the work in.

### Sources:

* [Colophon](!http://www.dictionary.com/browse/colophon?s=t)
* [Software Rot](!http://www.catb.org/jargon/html/S/software-rot.html)
* [Schema](!http://schema.org/docs/gs.html#schemaorg)
* [Virtualisation](!https://en.wikipedia.org/wiki/Virtualization)
* [Provisioning](!https://www.techopedia.com/definition/4069/provisioning-computing-computing)
* [Emulation](!https://en.wikipedia.org/wiki/Emulator)

### Further Reading

* [Will A Körner - What is the Future of Digital Art](!http://willakoerner.com/2014/11/20/what-is-the-future-of-digital-art/)
* [Leighton Walter Kille - The growing problem of Internet "link rot" and best practices for media and online publishers ](!https://journalistsresource.org/studies/society/internet/website-linking-best-practices-media-online-publishers)
