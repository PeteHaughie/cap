Introduction

Pete Haughie

Left college in 97, got a job in 4 colour process print making, learned Illustrator and Photoshop on PowerPC Macs as a Mac Artworker.

Moved from there to web when it was easy.

Gradually worked up into a major magazine publishers.

Started thinking artistically again about ten years ago. Moved to Portugal with my now wife and started in earnest. Started experimenting with DVD scripting (which already feels like very old technology) had a couple of shows, Sky (introduces the outside world to an interior space - if I was doing it now I'd do it in real time) and Dark Dancer (impossible choreography).

Moved to Leipzig, started expanding on those ideas. Created Doom Organ which I used to sneak into TV stores and let play on as many machines as possible.

Moved to Berlin because you would wouldn't you. Started expanding my practice, created Boner Lisa and some paintings of gifs blown up into larger format physical images, generative web stuff.

Moved to London started working on more occult based stuff, doubled down on the computer based stuff, created magick as a service (random number generator, elctricoven, twitter tarot).

Occasionally try to build quick things just to test ideas (gravehorrn, electric evil eye - although I try not to dip into other cultures or at least be aware I'm doing it).


TODAY: Want to move back into analogue for performance, maybe step into the limelight a little as I've always been behind the scenes with my installation, explore my ideas deeper and help to hone my approach

