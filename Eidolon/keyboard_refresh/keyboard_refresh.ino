uint8_t buf[8] = { 
  0 };   /* Keyboard report buffer */

//#define KEY_LEFT_CTRL  0x01
#define KEYCODE_F5 0x3E
#define PIN_REFRESH 7
#define PIN_LED 8

int state = 1;
int led_state = 1;

void setup() {
  Serial.begin(9600);
  pinMode(PIN_REFRESH, INPUT);
  digitalWrite(PIN_REFRESH, 1);
  pinMode(PIN_LED, INPUT);
  digitalWrite(PIN_LED, 0);
  delay(200);
}

void loop() {
  state = digitalRead(PIN_REFRESH);
  if (state != 1) {
    buf[0] = 0;   // Ctrl
    buf[2] = KEYCODE_F5;    // r
    digitalWrite(PIN_LED, 1);
    Serial.write(buf, 8); // Send keypress
    Serial.write('\n');
    releaseKey();
  }
}

void releaseKey() 
{
  buf[0] = 0;
  buf[2] = 0;
  digitalWrite(PIN_LED, 0);
  Serial.write(buf, 8);  // Release key  
}
