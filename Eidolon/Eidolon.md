# Eidolon

With Eidolon I am exploring playful methods of several ideas at the same time. My interests in Internet history, net art, and digital archival.

Some thoughts I had regarding this were:

* A respectful and non-pastiche way of contextualising the entire collection of midi files that were saved from the websites of the now defunct Geocities community, 31,000 of them in total
* Frame those files within the dates when Geocities was active, 1995 — 2009, via historical records, significant dates and events with photographs
* Show the possible symbiotic and mutually beneficial relationship between simulated environments and more modern platforms for preservation and display
* Show some of the difficulties of creating net art in the context of the timeframe by actually writing in a temporaneous language and syntax (I had actually forgotten how difficult it was…)

Currently the piece takes the form of a Raspberry Pi 3 host which is running the Python SimpleHTTPServer application and a QEMU machine image of a Windows 98 environment. This environment has a browser from somewhere within the timeframe. This browser then calls the host server to request a web page. This web page is a HTML4 table layout with a randomly selected embedded midi file set to autoplay. There is additional content in the form of images and text. These resources are generated as they are requested by the Windows client on the server.

The idea for this is adapted from something I originally saw at The National Museum of Computing at Bletchley Park. Sat in a back room is a machine that once-upon-a-time served in a control room monitoring the condition of a nuclear reactor. When it was retired, forgotten and then then reinstalled at TNMoC it was paired with a micro controller. This micro controller sends the computer data in the same format that it would have expected from reactor. In some sense it's living in a very simplistic virtual reality performing the function it was originally designed for in a digital Arcadian Utopia.

Initially I had started purely in Python but after some further rumination I realised that the platform and tone of delivery is just as important as the content I am attempting to deliver.

There is a low resolution CRT-style monitor connected to the rPi as well as a more modern LCD flat screen. The first screen displays the Windows environment and the second displays the host resource usage, generative script output, and live server requests.
The RPi sits in a semi-transparent case in the shape of an desktop IBM box. The case is a shadow of the archetypal object and indicative of the physical past but does not attempt to hide the reality of the host system.
Alongside the case is a matching keyboard with a single button to initiate the server request within the browser and a mouse which is just a box attached with some wire or string. There are some headphones nearby.
This installation should be sat in an office-like environment, corridor or dusty corner rather than in a gallery. A framed copy of the `GEOCITIES MIDI SWIPE` readme file should be sat on the desk.

Everything presented but for the partially obscured host system is from the past. The guest operating system, the browser the viewer is presented with, the choice of media delivery, the markup language used, the choice of a mechanical keyboard. But yet I am very aware that as Alfred Korzybski says "The map is not the territory". I'm not trying to recreate the past, just offer a portal into it briefly. Imperfect recollections, edited memories.

The documentation of the piece will also contain a colophon.md as an attempt to test my current thoughts on digital archival processes.
