#!/usr/bin/python
# -*- coding: utf-8 -*-

import SimpleHTTPServer
import SocketServer
import urlparse
import json
import re
import time
import random
import os
import threading

PORT = 8000

# variable dictionaries and strings

news = {
    '1995': [
        {
            'text': 'Austria, Finland and Sweden join the European Union.',
            'image': 'eu_flag.jpg'
        },
        {
           'text': 'Valeri Polyakov completes 366 days in space while aboard the Mir space station, breaking a duration record.',
            'image': 'cosmonaut_polyakov.jpg'
        },
        {
            'text': 'Prodigy begins offering access to the World Wide Web.',
            'image': 'prodigy_logo.jpg'
        },
        {
            'text': 'Opening statements in the O. J. Simpson murder case trial in Los Angeles.',
            'image': 'oj_simpson.jpg'
        },
        {
            'text': 'Hacker Kevin Mitnick is arrested by the FBI and charged with penetrating some of the United States\' most "secure" computer systems.',
            'image': 'kevin_mitnick.jpg'
        },
        {
            'text': 'The first Yahoo! Search interface is founded.',
            'image': 'yahoo.gif'
        },
        {
            'text': 'Microsoft releases Windows 95.',
            'image': 'windows95.jpg'
        },
        {
            'text': 'The DVD, an optical disc computer storage media format, is announced.',
            'image': 'dvd.gif'
        },
        {
            'text': 'eBay is founded.',
            'image': 'ebay.gif'

        }
    ],
    '1996': [
        {
            'text': ' Motorola introduces the Motorola StarTAC Wearable Cellular Telephone, the world\'s smallest and lightest mobile phone to date.',
            'image': 'startac.jpg'
        },
        {
            'text': 'France undertakes its last nuclear weapons test.',
            'image': 'nuke.jpg'
        },
        {
            'text': 'An amateur astronomer from southern Japan discovers Comet Hyakutake; it will pass very close to the Earth in March.',
            'image': 'comet.jpg'
        },
        {
            'text': 'The Telecommunications Act of 1996 was signed into law by U.S. President Bill Clinton, deregulating access to the telecommunications and broadcasting market, including the dissemination of media by internet. The bill was the first to be signed in cyberspace.',
            'image': 'deregulation.gif'
        },
        {
            'text': 'Chess computer "Deep Blue" defeats world chess champion Garry Kasparov for the first time.',
            'image': 'deep_blue.jpg'
        },
        {
            'text': 'Pokémon Red and Blue are released in Japan by Nintendo as Pocket Monsters: Red and Green, the first role-playing video game in the Pokémon series, developed by Game Freak.',
            'image': 'pokemon.jpg'
        },
        {
            'text': 'Dunblane massacre: Unemployed former shopkeeper Thomas Hamilton walks into the Dunblane Primary School in Scotland and opens fire, killing sixteen infant school pupils and one teacher before committing suicide.',
            'image': 'dunblaine.jpg'
        },
        {
            'text': 'The UK government announces that Bovine spongiform encephalopathy has likely been transmitted to people.',
            'image': 'bse.jpg'
        },
        {
            'text': 'Suspected "Unabomber" Theodore Kaczynski is arrested at his Montana cabin.',
            'image': 'unabomber.jpg'
        },
        {
            'text': 'The Nintendo 64 video game system is released in Japan.',
            'image': 'n64.jpg'
        },
        {
            'text': 'Dolly the sheep, the first mammal to be successfully cloned from an adult cell, is born at the Roslin Institute in Midlothian, Scotland, UK.',
            'image': 'dolly.jpg'
        },
        {
            'text': 'NASA announces that the Allan Hills 84001 meteorite, thought to originate from Mars, may contain evidence of primitive lifeforms',
            'image': 'ah84001.jpg'
        },
        {
            'text': 'The American punk rock band the Ramones perform for the final time.',
            'image': 'ramones.jpg'
        },
        {
            'text': 'News Corporation launches the Fox News Channel as a 24-hour news channel to compete against CNN.',
            'image': 'foxnews.jpg'
        },
        {
            'text': 'Al Jazeera, known as a multi-language news channel, begins regular service in Doha, Qatar.',
            'image': 'aljazeera.jpg'
        },
        {
            'text': 'Steve Jobs\' company NeXT is bought by Apple Computer, the company co-founded by Jobs.',
            'image': 'next.gif'
        }
    ],
    '1997': [
        {
            'text': 'The Emergency Alert System begins operation, having been approved in 1994.',
            'image': 'eas.gif'
        },
        {
            'text': 'The Comet Hale–Bopp makes its closest approach to Earth.',
            'image': 'halebopp.jpg'
        },
        {
            'text': 'In San Diego, 39 Heaven\'s Gate cultists commit mass suicide at their compound.',
            'image': 'heavensgate.jpg'
        },
        {
            'text': 'Channel 5 began broadcasting and was the fifth and final national terrestrial analogue network in the United Kingdom.',
            'image': 'channelfive.gif'
        },
        {
            'text': 'The popular children\'s television show Teletubbies debuts on BBC 2.',
            'image': 'teletubbies.jpg'
        },
        {
            'text': 'A Pegasus rocket carries the remains of 24 people into earth orbit, in the first space burial.',
            'image': 'spaceburial.jpg'
        },
        {
            'text': 'The Labour Party of the United Kingdom returns to power for the first time in 18 years, with Tony Blair becoming Prime Minister, in a landslide majority in the 1997 general election.',
            'image': 'blair.jpg'
        },
        {
            'text': 'A computer user known as "_eci" publishes his Microsoft C source code on a Windows 95 and Windows NT exploit, which later becomes WinNuke. The source code gets wide distribution across the internet, and Microsoft is forced to release a security patch.',
            'image': 'bsod.gif'
        },
        {
            'text': 'The fast food chain McDonald\'s wins a partial victory in its libel trial, known as the McLibel case, against two environmental campaigners.',
            'image': 'mclibel.jpg'
        },
        {
            'text': 'Bloomsbury Publishing publishes J. K. Rowling\'s Harry Potter and the Philosopher\'s Stone in London.',
            'image': 'harrypotter.jpg'
        },
        {
            'text': 'The United Kingdom hands sovereignty of Hong Kong to the People\'s Republic of China.',
            'image': 'hongkong.jpg'
        },
        {
            'text': 'NASA\'s Pathfinder space probe lands on the surface of Mars.',
            'image': 'pathfinder.gif'
        },
        {
            'text': 'In London, scientists report their DNA analysis findings from a Neanderthal skeleton, which support the out of Africa theory of human evolution, placing an "African Eve" at 100,000 to 200,000 years ago.',
            'image': 'mitochondrialeve.gif'
        },
        {
            'text': 'The F. W. Woolworth Company closes after 117 years in business.',
            'image': 'woolworth.gif'
        },
        {
            'text': 'Steve Jobs returns to Apple Computer, Inc at Macworld in Boston.',
            'image': 'apple.gif'
        },
        {
            'text': 'Microsoft buys a $150 million share of financially troubled Apple Computer.',
            'image': 'microsoft.gif'
        },
        {
            'text': 'The controversial animated sitcom South Park debuts on Comedy Central.',
            'image': 'southpark.gif'
        },
        {
            'text': 'Diana, Princess of Wales is taken to a hospital after a car accident shortly after midnight, in the Pont de l\'Alma road tunnel in Paris. She is pronounced dead at 3:00 am.',
            'image': 'diana.jpg'
        },
        {
            'text': 'Scotland votes to create its own Parliament after 290 years of union with England.',
            'image': 'scotland.gif'
        },
        {
            'text': 'British scientists Moira Bruce and John Collinge, with their colleagues, independently show that the new variant form of the Creutzfeldt–Jakob disease is the same disease as Bovine spongiform encephalopathy.',
            'image': 'cjd.jpg'
        },
        {
            'text': 'The remains of Che Guevara are laid to rest with full military honours in a specially built mausoleum in the city of Santa Clara, Cuba, where he had won the decisive battle of the Cuban Revolution 39 years before.',
            'image': 'che.jpg'
        },
        {
            'text': 'NASA launches the Cassini–Huygens probe to Saturn.',
            'image': 'cassinihuygens.jpg'
        },
        {
            'text': 'The Kyoto Protocol is adopted by a United Nations committee.',
            'image': 'kyoto.gif'
        },
        {
            'text': '"Dennō Senshi Porygon", an episode of the Pokémon TV series, is aired in Japan, inducing seizures in hundreds of Japanese children.',
            'image': 'siezure.gif'
        }
    ],
    '1998': [
        {
            "text": "Titanic becomes the first film to gross US$1 billion.",
            "image": "titanic.jpg"
        },
        {
            "text": "Data sent from the Galileo probe indicates that Jupiter's moon Europa has a liquid ocean under a thick crust of ice.",
            "image": "europa.gif"
        },
        {
            "text": "NASA announces that the Clementine probe orbiting the Moon has found enough water in polar craters to support a human colony and rocket fueling station.",
            "image": "clementine.jpg"
        },
        {
            "text": "Sildenafil, sold as Viagra and developed by Pfizer, is approved as the first oral treatment for erectile dysfunction in the USA by the Food and Drug Administration.",
            "image": "viagra.jpg"
        },
        {
            "text": "Netscape released Mozilla source code under an open source license. This is described in The Book of Mozilla, 3:31.",
            "image": "netscape.gif"
        },
        {
            "text": "Good Friday Agreement: 1 hour after the end of the talks deadline, the Belfast Agreement is signed between the Irish and British governments and most Northern Ireland political parties, with the notable exception of the Democratic Unionist Party.",
            "image": "stormont.jpg"
        },
        {
            "text": "The first euro coins are minted in Pessac, France. Because the final specifications for the coins were not finished in 1998, they will have to be melted and minted again in 1999.",
            "image": "euro.gif"
        },
        {
            "text": "<blink>Microsoft releases Windows 98.</blink>",
            "image": "windows98.jpg"
        },
        {
            "text": "The first RFID human implantation is tested in the United Kingdom.",
            "image": "rfid.jpg"
        },
        {
            "text": "Computer virus CIH activates and attacks Windows 9x.",
            "image": "cih.gif"
        },
        {
            "text": "Google, Inc. is founded in Menlo Park, California, by Stanford University PhD candidates Larry Page and Sergey Brin.",
            "image": "google.gif"
        },
        {
            "text": "Iranian President Mohammad Khatami retracts a fatwa against Satanic Verses author Salman Rushdie that was in force since 1989 stating that the Iranian government will \"neither support nor hinder assassination operations on Rushdie\".",
            "image": "satanicverses.jpg"
        },
        {
            "text": "British police place General Augusto Pinochet, the former Chilean dictator from 1973-1990, under house arrest during his medical treatment in the UK.",
            "image": "pinochet.jpg"
        },
        {
            "text": "Sesame Workshop (then known as Children's Television Workshop) launches Elmo's World, the most famous Sesame Street segment, lasting from the late 90' to Sesame Street's 40th anniversary (69-09) and the 10th anniversary of the segment itself (99-09).",
            "image": "elmo.gif"
        },
        {
            "text": "Nintendo releases The Legend of Zelda: Ocarina of Time.",
            "image": "zelda.jpg"
        }
    ],
    # '1999': '',
    # '2000': '',
    # '2001': '',
    # '2002': '',
    # '2003': '',
    # '2004': '',
    # '2005': '',
    # '2006': '',
    # '2007': '',
    # '2008': '',
    # '2009': ''
}

color = {
    'BLACK': '#000000',
    'SILVER': '#C0C0C0',
    'GRAY': '#808080',
    'WHITE': '#FFFFFF',
    'MAROON': '#800000',
    'RED': '#FF0000',
    'PURPLE': '#800080',
    'FUCHSIA': '#FF00FF',
    'GREEN': '#008000',
    'LIME': '#00FF00',
    'OLIVE': '#808000',
    'YELLOW': '#FFFF00',
    'NAVY': '#000080',
    'BLUE': '#0000FF',
    'TEAL': '#008080',
    'AQUA': '#00FFFF'
}

def defineColor():
    colorVal = random.randrange(len(color))
    colorKey = color.items()[colorVal][0]
    return colorVal, colorKey

# print defineColor()[1] # lets get the ugly color names for added painpoints

def defineYear():
    # year = random.randint(1995, 2009)
    year = random.randint(1995, 1997)
    return year

genYear = defineYear() # specify a year from the range above

def defineNews(genYear):
    newsVal = random.randrange(len(news[str(genYear)]))
    newsEntry = news[str(genYear)][newsVal]
    newsText = newsEntry["text"]
    newsImage = newsEntry["image"]
    return newsText, newsImage

newsLen = len(news[str(genYear)])

midiFolders = [
    "_",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z"
]

def defineMidi():
    folder = random.choice(midiFolders)
    midi = random.choice(os.listdir("./www/midi/" + folder))
    fileStr = "./midi/" + folder + "/" + midi
    return fileStr

def generateIndex():
    genYear = defineYear() # specify a year from the range above
    print genYear
    # print news[str(genYear)]
    newsLen = len(news[str(genYear)])
    print newsLen
    genNews = defineNews(genYear)
    print genNews
    imageURL = "images/" + str(genYear) + "/" + genNews[1]
    cleanURL = imageURL.replace('//','/')
    print imageURL, cleanURL
    midiFileName = defineMidi()
    print midiFileName
    genCol1 = defineColor()[1]
    genCol2 = defineColor()[1]
    genCol3 = defineColor()[1]
    genCol4 = defineColor()[1]
    txtCol2 = "BLACK"
    txtCol3 = "BLACK"
    if genCol2 == "BLACK":
        txtCol2 = "WHITE"
    if genCol3 == "BLACK":
        txtCol3 = "WHITE"
    # body parts
    head = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n\"http://www.w3.org/TR/html4/loose.dtd\">\n<HTML>\n<HEAD>\n<TITLE>Eidolon - " + str(genYear) + "</TITLE>\n</HEAD>"
    body1 = "<BODY BGCOLOR=" + genCol1 + ">\n<TABLE CELLSPACING=0 CELLPADDING=0 BORDER=1 WIDTH=100% HEIGHT=100%>\n<TR>\n<TD BGCOLOR=" + genCol2 + "><H1><MARQUEE><FONT COLOR=" + txtCol2 + ">EIDOLON - " + str(genYear) + "</FONT></MARQUEE></H1></TD>\n</TR><TR>\n"
    body2 = "\n</TR>\n</TABLE>\n"
    foot = "</BODY>\n</HTML>"
    f = open("./www/index.html", "w")
    f.write(head)
    f.write(body1)
    f.write("<TD WIDTH=100% BGCOLOR=")
    f.write(genCol3)
    f.write(" ALIGN=CENTER>\n")
    f.write("<FONT COLOR=")
    f.write(txtCol3)
    f.write(">\n")
    f.write(genNews[0])
    f.write("\n")
    f.write("</FONT>")
    f.write("</TD>\n")
    f.write("</TR>\n")
    f.write("<TR>\n")
    f.write("<TD WIDTH=100% BGCOLOR=")
    f.write(genCol4)
    f.write(" ALIGN=CENTER>\n")
    f.write("<IMG SRC='" + cleanURL + "'>\n")
    f.write("\n")
    f.write("</TD>\n")
    f.write("</TR>\n")
    f.write(body2)
    f.write("<EMBED SRC='" + midiFileName + "' AUTOSTART=TRUE AUTOPLAY=TRUE HIDDEN='TRUE'")
    f.write(foot)
    f.close()

# print generateIndex() # debug the index
generateIndex()

# exit() # uncomment this to send kill command to the server for testing variables

# This class will handles any incoming request from
# the browser
class myHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

  def send_success(self, content_type):
    self.send_response(200)
    self.send_header('Content-type', content_type)
    self.end_headers()

  def read_file_from_path(self, path):
    return open(path, 'r').read()

  # Handler for the GET requests
  def do_GET(self):
    if self.path == '/':
      generateIndex()
      self.send_success('text/html')
      self.wfile.write(self.read_file_from_path('./www/index.html'))
    elif re.search('.*\.js', self.path):
      self.send_success('text/javascript')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.jpg', self.path):
      self.send_success('application/image-jpg')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.jpeg', self.path):
      self.send_success('application/image-jpeg')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.gif', self.path):
      self.send_success('image/gif')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.mid', self.path):
      self.send_success('application/midi ')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.MID', self.path):
      self.send_success('application/midi ')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    elif re.search('.*\.Mid', self.path):
      self.send_success('application/midi ')
      self.wfile.write(self.read_file_from_path('./www' + self.path))
    else:
      self.send_response(404)
      self.end_headers()

    return

httpd = SocketServer.TCPServer(("", PORT), myHandler)

print "serving at port", PORT
httpd.serve_forever()
