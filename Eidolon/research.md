## Create Disk Image

`hdiutil create -megabytes 512 -fs MS-DOS -volname win98 -o`

https://rubenerd.com/qemu-ad-lib-midi-win-31/


## QEMU

Couldn't get this working sadly, install went well, sound was great. No networking.

QEMU documentation - https://qemu.weilnetz.de/doc/qemu-doc.html

## What did work?

Installing Windows 98 in VirtualBox and the converting it to a QEMU qcow2 format with `qemu-img convert -f vdi -O qcow2 image.vdi image.img`

## Single Key Keyboard

Say what? You hear me. All it does is send an `f5` signal to the Windows box.

http://mitchtech.net/arduino-usb-hid-keyboard/
https://github.com/nylen/auto-f1-presser

## ATX Case

Just a massive case for a raspberry pi.

http://boxdesigner.connectionlab.org/
