// import libs
import processing.sound.*;

// globals
SoundFile[] files;
PImage[] tuning = new PImage[11];
PImage[] vhsStatic = new PImage[4];
PImage[] vhsTracking = new PImage[10];
PImage[] leftEye = new PImage[45];
PImage[] leftEyeLookLeft = new PImage[73];
PImage[] leftEyeLookRight = new PImage[53];
PImage[] rightEye = new PImage[45];
PImage[] rightEyeLookLeft = new PImage[73];
PImage[] rightEyeLookRight = new PImage[53];
PImage[] leftEyeBlue = new PImage[45];
PImage[] leftEyeLookLeftBlue = new PImage[73];
PImage[] leftEyeLookRightBlue = new PImage[53];
PImage[] rightEyeBlue = new PImage[45];
PImage[] rightEyeLookLeftBlue = new PImage[73];
PImage[] rightEyeLookRightBlue = new PImage[53];
PImage img = new PImage();

// iterators
int loop_audio;
int loop_tuning;
int draw_tuning;
int loop_vhsStatic;
int draw_vhsStatic;
int loop_vhsTracking;
int draw_vhsTracking;
int loop_leftEye;
int draw_leftEye;
int loop_leftEyeLookLeft;
int draw_leftEyeLookLeft;
int loop_leftEyeLookRight;
int draw_leftEyeLookRight;
int loop_rightEye;
int draw_rightEye;
int loop_rightEyeLookLeft;
int draw_rightEyeLookLeft;
int loop_rightEyeLookRight;
int draw_rightEyeLookRight;
// we only need the loops for these because we want the draw state to remain the same 
// in fact we don't even need these because we know how many images there are already
// for each one - still, it pays to be thorough
int loop_leftEyeBlue;
int loop_leftEyeLookLeftBlue;
int loop_leftEyeLookRightBlue;
int loop_rightEyeBlue;
int loop_rightEyeLookLeftBlue;
int loop_rightEyeLookRightBlue;
int whichSoundIsPlaying = 0;

// Rando Calrissian
int RNG = 0;
int PrevRNG = 0;

// machine states
boolean sound1 = false;
boolean sound2 = false;
boolean sound3 = false;
boolean sound4 = false;
boolean sound5 = false;

boolean playLeftEye = true;
boolean playLeftEyeLookLeft = false;
boolean playLeftEyeLookRight = false;

boolean playRightEye = true;
boolean playRightEyeLookLeft = false;
boolean playRightEyeLookRight = false;

// setters
int p_value = 9;
int i_width = (width / 2);// lol eye see what you did thar

// audio
int[] playSound = { 
  1, 1, 1, 1, 1
};

// an idea
int[] sounds = {0, 0, 0, 0, 0}; // This is an array that keeps track of what's playing and what's not (0 if it's off, 1 if it's on)

void startPlayingSound(int soundToPlay) {
  // Look to see if any sounds are playing by looping over everything in the library (index of a sound playing will be 1)
  for (int i = 0; i < sounds.length; i++) {
    // If you find an index set to 1, note what it is (for some reason you can't call file.stop() from this far down in a loop)
    if (sounds[i] == 1) {
      files[i].stop();
    }
  }
  // The good stuff:
  // We know what sound we want to play. Set its index to 1 so we know it's going next time we look: 
  sounds[soundToPlay] = 1;
  // Start that fucker playing:
  files[soundToPlay].play();
}

void chooseRightEye(){
  playRightEye = false;
  playRightEyeLookLeft = false;
  playRightEyeLookRight = false;
  float r = random(5);
  if (r < 3) {
    playRightEye = true;
  }
  if (r > 3 && r < 4) {
    playRightEyeLookLeft = true;
  }
  if (r > 4) {
    playRightEyeLookRight = true;
  }
}

void chooseLeftEye(){
  playLeftEye = false;
  playLeftEyeLookLeft = false;
  playLeftEyeLookRight = false;
  float r = random(5);
  if (r < 3) {
    playLeftEye = true;
  }
  if (r > 3 && r < 4) {
    playLeftEyeLookLeft = true;
  }
  if (r > 4) {
    playLeftEyeLookRight = true;
  }
}

// and on with the show
void setup(){
  // scene parameters
  frameRate(14);
  size(640,480); // for testing
  //noCursor();
  //fullScreen(); // need to work out how to resize the image loops before you do this

  // populate audio array
  files = new SoundFile[5];
  for (loop_audio = 0; loop_audio < files.length; loop_audio++) {
    files[loop_audio] = new SoundFile(this, "sound" + loop_audio + ".wav");
  }

  // interstitial loops
  for (loop_tuning = 0; loop_tuning < tuning.length; loop_tuning ++) {
    tuning[loop_tuning] = loadImage("tv_tuning-" + loop_tuning + ".gif");
  }

  for (loop_vhsStatic = 0; loop_vhsStatic < vhsStatic.length; loop_vhsStatic ++) {
    vhsStatic[loop_vhsStatic] = loadImage("vhs_static-" + loop_vhsStatic + ".gif");
  }

  for (loop_vhsTracking = 0; loop_vhsTracking < vhsTracking.length; loop_vhsTracking ++) {
    vhsTracking[loop_vhsTracking] = loadImage("vhs_tracking-" + loop_vhsTracking + ".gif");
  }

  // leftEye image array
  for (loop_leftEye = 0; loop_leftEye < leftEye.length; loop_leftEye ++) {
    leftEye[loop_leftEye] = loadImage("left_eye-" + loop_leftEye + ".gif");
  }

  // leftEyeLookLeft
  for (loop_leftEyeLookLeft = 0; loop_leftEyeLookLeft < leftEyeLookLeft.length; loop_leftEyeLookLeft ++) {
    leftEyeLookLeft[loop_leftEyeLookLeft] = loadImage("left_eye_look_left-" + loop_leftEyeLookLeft + ".gif");
  }

  // leftEyeLookRight
  for (loop_leftEyeLookRight = 0; loop_leftEyeLookRight < leftEyeLookRight.length; loop_leftEyeLookRight ++) {
    leftEyeLookRight[loop_leftEyeLookRight] = loadImage("left_eye_look_right-" + loop_leftEyeLookRight + ".gif");
  }

  // rightEye image array
  for (loop_rightEye = 0; loop_rightEye < rightEye.length; loop_rightEye ++) {
    rightEye[loop_rightEye] = loadImage("right_eye-" + loop_rightEye + ".gif");
  }

  // rightEyeLookLeft
  for (loop_rightEyeLookLeft = 0; loop_rightEyeLookLeft < rightEyeLookLeft.length; loop_rightEyeLookLeft ++) {
    rightEyeLookLeft[loop_rightEyeLookLeft] = loadImage("right_eye_look_left-" + loop_rightEyeLookLeft + ".gif");
  }

  // rightEyeLookRight
  for (loop_rightEyeLookRight = 0; loop_rightEyeLookRight < rightEyeLookRight.length; loop_rightEyeLookRight ++) {
    rightEyeLookRight[loop_rightEyeLookRight] = loadImage("right_eye_look_right-" + loop_rightEyeLookRight + ".gif");
  }
  // same as above but the blue images
  // leftEyeBlue image array
  for (loop_leftEyeBlue = 0; loop_leftEyeBlue < leftEyeBlue.length; loop_leftEyeBlue ++) {
    leftEyeBlue[loop_leftEyeBlue] = loadImage("left_eye_blue-" + loop_leftEyeBlue + ".gif");
  }

  // leftEyeLookLeftBlue
  for (loop_leftEyeLookLeftBlue = 0; loop_leftEyeLookLeftBlue < leftEyeLookLeftBlue.length; loop_leftEyeLookLeftBlue ++) {
    leftEyeLookLeftBlue[loop_leftEyeLookLeftBlue] = loadImage("left_eye_look_left_blue-" + loop_leftEyeLookLeftBlue + ".gif");
  }

  // leftEyeLookRightBlue
  for (loop_leftEyeLookRightBlue = 0; loop_leftEyeLookRightBlue < leftEyeLookRightBlue.length; loop_leftEyeLookRightBlue ++) {
    leftEyeLookRightBlue[loop_leftEyeLookRightBlue] = loadImage("left_eye_look_right_blue-" + loop_leftEyeLookRightBlue + ".gif");
  }

  // rightEyeBlue image array
  for (loop_rightEyeBlue = 0; loop_rightEyeBlue < rightEyeBlue.length; loop_rightEyeBlue ++) {
    rightEyeBlue[loop_rightEyeBlue] = loadImage("right_eye_blue-" + loop_rightEyeBlue + ".gif");
  }

  // rightEyeLookLeftBlue
  for (loop_rightEyeLookLeftBlue = 0; loop_rightEyeLookLeftBlue < rightEyeLookLeftBlue.length; loop_rightEyeLookLeftBlue ++) {
    rightEyeLookLeftBlue[loop_rightEyeLookLeftBlue] = loadImage("right_eye_look_left_blue-" + loop_rightEyeLookLeftBlue + ".gif");
  }

  // rightEyeLookRightBlue
  for (loop_rightEyeLookRightBlue = 0; loop_rightEyeLookRightBlue < rightEyeLookRightBlue.length; loop_rightEyeLookRightBlue ++) {
    rightEyeLookRightBlue[loop_rightEyeLookRightBlue] = loadImage("right_eye_look_right_blue-" + loop_rightEyeLookRightBlue + ".gif");
  }
}

void draw(){
  // the left eye
  if (RNG == 0){
    if (playLeftEye) {
      if (loop_leftEye > draw_leftEye) {
        leftEye[draw_leftEye].resize(width/2, height);
        image(leftEye[draw_leftEye], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEye++;
      } else {
        draw_leftEye = 0;
        chooseLeftEye();
      }
    }
  
    if (playLeftEyeLookLeft) {
      if (loop_leftEyeLookLeft > draw_leftEyeLookLeft) {
        leftEyeLookLeft[draw_leftEyeLookLeft].resize(width/2, height);
        image(leftEyeLookLeft[draw_leftEyeLookLeft], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEyeLookLeft++;
      } else {
        draw_leftEyeLookLeft = 0;
        chooseLeftEye();
      }
    }
  
    if (playLeftEyeLookRight) {
      if (loop_leftEyeLookRight > draw_leftEyeLookRight) {
        leftEyeLookRight[draw_leftEyeLookRight].resize(width/2, height);
        image(leftEyeLookRight[draw_leftEyeLookRight], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEyeLookRight++;
      } else {
        draw_leftEyeLookRight = 0;
        chooseLeftEye();
      }
    }
  
    // the right eye
    if (playRightEye) {
      if (loop_rightEye > draw_rightEye) {
        rightEye[draw_rightEye].resize(width/2, height);
        image(rightEye[draw_rightEye], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEye++;
      } else {
        draw_rightEye = 0;
        chooseRightEye();
      }
    }
  
    if (playRightEyeLookLeft) {
      if (loop_rightEyeLookLeft > draw_rightEyeLookLeft) {
        rightEyeLookLeft[draw_rightEyeLookLeft].resize(width/2, height);
        image(rightEyeLookLeft[draw_rightEyeLookLeft], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEyeLookLeft++;
      } else {
        draw_rightEyeLookLeft = 0;
        chooseRightEye();
      }
    }
  
    if (playRightEyeLookRight) {
      if (loop_rightEyeLookRight > draw_rightEyeLookRight) {
        rightEyeLookRight[draw_rightEyeLookRight].resize(width/2, height);
        image(rightEyeLookRight[draw_rightEyeLookRight], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEyeLookRight++;
      } else {
        draw_rightEyeLookRight = 0;
        chooseRightEye();
      }
    }
  }
  // same but blue images
  if (RNG == 1){
    if (playLeftEye) {
      if (loop_leftEyeBlue > draw_leftEye) {
        leftEyeBlue[draw_leftEye].resize(width/2, height);
        image(leftEyeBlue[draw_leftEye], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEye++;
      } else {
        draw_leftEye = 0;
        chooseLeftEye();
      }
    }
  
    if (playLeftEyeLookLeft) {
      if (loop_leftEyeLookLeftBlue > draw_leftEyeLookLeft) {
        leftEyeLookLeftBlue[draw_leftEyeLookLeft].resize(width/2, height);
        image(leftEyeLookLeftBlue[draw_leftEyeLookLeft], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEyeLookLeft++;
      } else {
        draw_leftEyeLookLeft = 0;
        chooseLeftEye();
      }
    }
  
    if (playLeftEyeLookRight) {
      if (loop_leftEyeLookRightBlue > draw_leftEyeLookRight) {
        leftEyeLookRightBlue[draw_leftEyeLookRight].resize(width/2, height);
        image(leftEyeLookRightBlue[draw_leftEyeLookRight], 0, 0);
        filter(POSTERIZE, p_value);
        draw_leftEyeLookRight++;
      } else {
        draw_leftEyeLookRight = 0;
        chooseLeftEye();
      }
    }
  
    // the right eye
    if (playRightEye) {
      if (loop_rightEyeBlue > draw_rightEye) {
        rightEyeBlue[draw_rightEye].resize(width/2, height);
        image(rightEyeBlue[draw_rightEye], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEye++;
      } else {
        draw_rightEye = 0;
        chooseRightEye();
      }
    }
  
    if (playRightEyeLookLeft) {
      if (loop_rightEyeLookLeftBlue > draw_rightEyeLookLeft) {
        rightEyeLookLeftBlue[draw_rightEyeLookLeft].resize(width/2, height);
        image(rightEyeLookLeftBlue[draw_rightEyeLookLeft], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEyeLookLeft++;
      } else {
        draw_rightEyeLookLeft = 0;
        chooseRightEye();
      }
    }
  
    if (playRightEyeLookRight) {
      if (loop_rightEyeLookRightBlue > draw_rightEyeLookRight) {
        rightEyeLookRightBlue[draw_rightEyeLookRight].resize(width/2, height);
        image(rightEyeLookRightBlue[draw_rightEyeLookRight], width/2, 0);
        filter(POSTERIZE, p_value);
        draw_rightEyeLookRight++;
      } else {
        draw_rightEyeLookRight = 0;
        chooseRightEye();
      }
    }
  }

  if (RNG == 2) {
    // the vhs tracking
    if (PrevRNG != 2){
      startPlayingSound(4);
    }
    if (loop_vhsTracking > draw_vhsTracking) {
      vhsTracking[draw_vhsTracking].resize(width, height);
      image(vhsTracking[draw_vhsTracking], 0, 0);
      draw_vhsTracking++;
    } else {
      draw_vhsTracking = 0;
    }
    PrevRNG = 2;
  }

  if (RNG == 3) {
    // the vhs static
    if (PrevRNG != 3){
      startPlayingSound(1);
    }
    if (loop_vhsStatic > draw_vhsStatic) {
      vhsStatic[draw_vhsStatic].resize(width, height);
      image(vhsStatic[draw_vhsStatic], 0, 0);
      draw_vhsStatic++;
    } else {
      draw_vhsStatic = 0;
    }
    PrevRNG = 3;
  }

  if (RNG == 4) {
    // the tuning screen
    if (PrevRNG != 4){
      startPlayingSound(3);
    }
    if (loop_tuning > draw_tuning) {
      tuning[draw_tuning].resize(width, height);
      image(tuning[draw_tuning], 0, 0);
      draw_tuning++;
    } else {
      draw_tuning = 0;
    }
    PrevRNG = 4;
  }
}

void mousePressed(){
  PrevRNG = RNG;
  RNG = int(random(5));
  startPlayingSound(0);
}