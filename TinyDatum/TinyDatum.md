# Tiny Datum

Sing like no one is listening. Idiot Parrot.

A pair of identical shiny black plastic boxes with bulbs set in their tops. Set up at opposing sides of a gallery space. Both boxes are plugged into an electrical outlet on long braided lamp cables. Both boxes are constantly sampling their local environments with proximity, body heat, decibel, and light sensors. Each box reacts differently to these limited stimuli.

Box one, does nothing if there are people nearby, the lights are too bright, or there's too much noise. Its light remains resolutely off and it is seemingly completely dormant. When the conditions are just right however it will pulse its little lamp and hum to itself as if content.

Box two, also does nothing. Until there are people nearby who are making noise. When this is the case then it begins to flash its light rapidly and attempt to 'shout' over them at a slightly higher volume than them until they shut up.

It is my hope that people will enjoy attempting to annoy box two and leave box one in peace to do his thing. If people move towards box one, both boxes remain inert, if they start to interact with box two then the pair of them come to 'life'.

## Bill of Parts

* 2 x Arduino Uno
* 2 x Microphone Sensors
* 2 x Ultrasonic Proximity Sensors
* 2 x Passive IR Sensors
* 2 x Arduino Wave Shields
* 2 x 20W 8Ohm Speakers
* 2 x Breadboards
* 2 x Relay Switches
* 2 x Lamp Sockets
* 2 x Dimmable LED Bulbs
* 2 x 2m Lengths of Lamp Cable
* 2 x Black Perspex Boxes (to be designed and laser cut)
