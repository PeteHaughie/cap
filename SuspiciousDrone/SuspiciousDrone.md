# Suspicious Drone

A series of found sounds. Small field recordings given prominence and significance through exploration and expansion.

Not an album of tracks as such but a series of experiences where one can retexturise and reconfigure the playback in realtime via a process of haptic interrogation.

## Other Examples

Sleep Research Facility - Nostromo - https://en.wikipedia.org/wiki/SleepResearch_Facility

Buddha Machine F3 - http://www.fm3buddhamachine.com/v2/
